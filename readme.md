# Sales Absent API

Model:
- Absent
  * userId
  * longitude
  * latitude
  * absenId
  * submitDate
  * submitTime
  * pictureTakenTime
  * pictureTakenDate
  * picture
- Sales
  * base class User

Relation
* Absent belong to Sales without foreign key


ACL
* Deny everyone all endpoints
* Allow authenticated users to create an Absent
* Allow authenticated users to get Absent List

