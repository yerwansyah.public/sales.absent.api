'use strict';

module.exports = function (Absent) {
    Absent.beforeRemote('create', function (context, user, next) {
        context.args.data.userId = context.req.accessToken.userId;
        next();
    });
};
