var async = require('async');
module.exports = function (app) {
    //data sources
    var mongoDs = app.dataSources.mongoDs;
    //create all models
    async.parallel({
        employee: async.apply(createEmployees)
    }, function (err, results) {
        if (err) throw err;
        createEmployees(results.employee, function (err) {
            console.log('> models created sucessfully');
        });
    });

    //create sales
    function createEmployees(cb) {
        mongoDs.automigrate('employee', function (err) {
            if (err) return cb(err);

            var employee = app.models.employee;
            employee.create([{
                username: 'admin',
                email: 'bootcamp.intikom@gmail.com',
                password: 'Intikom11'
            }, {
                username: 'yerwansyah',
                email: 'yerwansyahm@gmail.com',
                password: 'Intikom11'
            }], cb);
        });
    }
};